package main

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"
	"time"
)

func scoreWords(a,b,c [5]rune, freq map[rune]int, score int, antiDouble *[256]bool) int {		
	for _, c := range c {
		if !antiDouble[c] {
			antiDouble[c] = true
			score += freq[c]
		}
	}

	return score
}

func findBestWords(freq map[rune]int) {
	b, _ := ioutil.ReadFile("words_full2")
	unfilteredWords := strings.Split(string(b), " ")
	bestWord1 := ""
	bestWord2 := ""
	bestWord3 := ""
	bestScore := 0
	
	words := make([][5]rune, 0, len(unfilteredWords))

	antiDouble := make(map[rune]struct{}, 5)
words_loop:
	for _, w := range unfilteredWords {
		antiDouble = make(map[rune]struct{}, 5)
		for _, c := range string(w) {
			if _, ok := antiDouble[c]; ok {
				continue words_loop
			}
			antiDouble[c] = struct{}{}
		}
		var runes [5]rune
		copy(runes[:], []rune(w))
		words = append(words, runes)
	}
	fmt.Println(len(unfilteredWords), "TO", len(words))

	for i1, w1 := range words {
		fmt.Printf("w1: %s", string(w1[:]))
		t := time.Now()
		score := 0
		var antiDouble [256] bool
		
		for _, c := range w1 {
			antiDouble[c] = true
			score += freq[c]
		}

		for i2, w2 := range words[i1:] {
			antiDouble2 := antiDouble
			score2 := score
			for _, c := range w2 {
				if !antiDouble2[c] {
					antiDouble2[c] = true
					score2 += freq[c]
				}
			}
			for _, w3 := range words[i1+i2:] {
				antiDouble3 := antiDouble2
				score3 := score2
				for _, c := range w3 {
					if !antiDouble3[c] {
						antiDouble3[c] = true
						score3 += freq[c]
					}
				}
				if score3 > bestScore {
					bestWord1 = string(w1[:])
					bestWord2 = string(w2[:])
					bestWord3 = string(w3[:])
					bestScore = score3
				}
			}
		}
		fmt.Printf(" in %s current best: %d with w1:%s, w2:%s, w3:%s\n", time.Since(t), bestScore, bestWord1, bestWord2, bestWord3)
	}
	fmt.Println(bestWord1, bestWord2, bestWord3, bestScore)
}

func main(){
	b, _ := ioutil.ReadFile("words")
	
	antiDouble := make(map[rune]struct{},5)
	freq := make(map[rune]int,26)
	
	for _, c := range string(b) {
		if c == ' ' {
			antiDouble = make(map[rune]struct{}, 5)	
		} else if _, ok := antiDouble[c]; !ok {
			antiDouble[c] = struct{}{}
			freq[c]++
		}
	}
	ars := make([]rune, 0, len(freq))
	for c, _ := range freq {
		ars = append(ars, c)
	}
	sort.Slice(ars, func(i,j int) bool{
		return freq[ars[i]] > freq[ars[j]]
	})

	for _, s := range ars {
		fmt.Println(string(s), freq[s])
	}

	findBestWords(freq)
}
